<?php
// ================================= PATHS =======================================

define("ROOT_PATH",dirname(__FILE__));                               // đường dẫn thư mục chính
define("LIB_PATH",ROOT_PATH . "/libs/");                             // đường đẫn đến thư mục thư viện
define("PUBLIC_PATH",ROOT_PATH . "/public/");                        // đường đẫn đến thư mục public
define("PUBLIC_FILE_PATH",PUBLIC_PATH . "files/");                  // đường đẫn đến thư mục public /files
define("APPLICATION_PATH",ROOT_PATH . "/application/");              // đường đẫn đến thư mục application
define("MODULE_PATH",APPLICATION_PATH . "/module/");                 // đường đẫn đến thư mục module
define("TEMPLATE_PATH",PUBLIC_PATH . "template/");                   // đường đẫn đến thư mục template
define("BLOCK_PATH",APPLICATION_PATH . "block/");                    // đường đẫn đến thư mục template
define("TEMPLATE_LINK_PATH","./public/template/");                   // đường đẫn đến thư mục template tam thoi
define("APPLICATION_LINK_PATH","./application/");                    // đường đẫn đến thư mục application tam thoi
define("TEMPLATE_LIB_EXTEND_PATH","./libs/extends/");                // đường đẫn đến thư mục thư viện/extends
define("TEMPLATE_SCRIPTS_PATH","./public/scripts/");                 // đường đẫn đến thư mục thư viện/extends
define("TEMPLATE_FILE_PATH","./public/files/");                      // đường đẫn đến thư mục images category


define("DEFAULT_MODULE","default");
define("DEFAULT_CONTROLLER","index");
define("DEFAULT_ACTION","index");

// ================================= DB ===========================================

define("DB_HOST","database");
define("DB_USER","root");
define("DB_PASSWORD","password");
define("DB_NAME","volunteer");
define("DB_TABLE","THANHVIEN");

define('DB_TABLE_THANHVIEN', 'THANHVIEN');
define('DB_TABLE_HOATDONG', 'HOATDONG');
define('DB_TABLE_THAMGIA', 'THAMGIA');


// ================================= CONFIG ===========================================
define('TIME_LOGIN', 3600);
define('MIN_FILE',100);
define('MAX_FILE',5242880);
define('EXTENSION_FILE',"jpg||png||zip||mp3||jpeg||JPG");