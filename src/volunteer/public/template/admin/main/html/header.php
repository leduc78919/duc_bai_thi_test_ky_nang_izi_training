<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="refresh" content="3600">  sau 1h se tu dong refesh trang 1 lan  -->
    <?php echo $this->meta_http;
            echo $this->meta_name;
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="zend"> -->
    <title>BOOK STORE - ADMIN </title>
    <?php //echo $this->title;
        echo $this->css;
        echo $this->js;
    ?>
    <!-- <link rel="stylesheet" type="text/css" href="css/template.css"/>
    <link rel="stylesheet" type="text/css" href="css/system.css"/> -->
    
</head>
<body>
<?php 
    $link_control_panel   = url::create_url("admin","index","index");
    $link_my_profile      = url::create_url("admin","index","profile");
    $link_user_manager    = url::create_url("admin","user","index");
    $link_add_user        = url::create_url("admin","user","form");
    $link_log_out         = url::create_url("admin","index","logout");
    $link_group_manager   = url::create_url("admin","group","index");
    $link_add_group       = url::create_url("admin","group","form");
    $link_categories      = url::create_url("admin","category","index");
    $link_add_category    = url::create_url("admin","category","form");
    $link_view_site       = url::create_url("default","index","index");
    $link_book            = url::create_url("admin","book","index");
    $link_add_book        = url::create_url("admin","book","form");
    $link_cart_manager    = url::create_url("admin","cart","index");

?>
<div id="border-top" class="h_blue">
		<span class="title"><a href="#">Administration</a></span>
	</div>
	
    <!-- HEADER -->
	<div id="header-box">
		<div id="module-status">
            <span class="viewsite"><a href="<?php echo @$link_view_site; ?>" target="_blank">View Site</a></span>
            <span class="no-unread-messages"><a href="<?php echo $link_log_out ?>">Log out</a></span> 
		</div>
        <div id="module-menu">
        	<!-- MENU -->
            <ul id="menu" >
                <li class="node"><a href="#">Site</a>
                    <ul>
                        <li><a class="icon-16-cpanel" href="<?php echo $link_control_panel; ?>">Control Panel</a></li>
                        <li class="separator"><span></span></li>
                        <li><a class="icon-16-profile" href="<?php echo $link_my_profile; ?>">My Profile</a></li>
                        <li class="separator"><span></span></li>
                        <li><a class="icon-16-config" href="#">Global Configuration</a></li>
                        <li class="separator"><span></span></li>
                        <li class="node"><a class="icon-16-maintenance" href="#">Maintenance</a>
                            <ul id="menu-com-checkin" class="menu-component">
                                <li><a class="icon-16-checkin" href="#">Global Check-in</a></li>
                                <li class="separator"><span></span></li>
                                <li><a class="icon-16-clear" href="#">Clear Cache</a></li>
                                <li><a class="icon-16-purge" href="#">Purge Expired Cache</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="separator"><span></span></li>
                <li class="node"><a href="#">Users</a>
                    <ul>
                        <li class="node">
                            <a href="<?php echo $link_user_manager; ?>" class = "icon-16-user">User Manager</a>
                            <ul id="menu-com-users-users" class="menu-component">
                                <li>
                                    <a href="<?php echo $link_add_user; ?>" class="icon-16-newarticle">Add New Users</a>
                                </li>
                            </ul>
                        </li>
                        <li class="node">
                            <a href="<?php echo $link_group_manager; ?>" class="icon-16-user">Groups</a>
                            <ul id="menu-com-users-users" class="menu-component">
                                <li>
                                    <a href="<?php echo $link_add_group; ?>" class="icon-16-newarticle">Add New Group</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="node"><a href="#">Book Shopping</a>
                    <ul>
                        <li class="node">
                            <a href="<?php echo $link_categories; ?> " class ="icon-16-category">Category Manager</a>
                            <ul id="menu-com-users-users" class="menu-component">
                                <li>
                                    <a href="<?php echo $link_add_category; ?>" class="icon-16-newarticle">Add New Category</a>
                                </li>
                            </ul>
                        </li>
                        <li class="node">
                        <a href="<?php echo $link_book; ?>" class = "icon-16-article">Book Manager</a>
                            <ul id="menu-com-users-users" class="menu-component">
                                <li>
                                    <a href="<?php echo $link_add_book; ?>" class="icon-16-newarticle">Add New Book</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    
		<div class="clr"></div>
	</div>
    