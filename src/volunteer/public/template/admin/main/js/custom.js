function change_status(url)
{
    $.get(url,function(data)
    {
        var id = data[0]
        var status = data[1]
        var link = data[2]
        var class_remove = "state unpublish";
        var class_add   = "state publish";
        if(status == 0)
        {
            class_remove = "state publish"
            class_add   = "state unpublish";
        }
        $('a#status-'+id + ' span').removeClass(class_remove).addClass(class_add)
        $('a#status-'+id).attr('href','javascript:change_status(\' '+ link +'\')')
    },"json")
}

function change_confirm(url)
{
    $.get(url,function(data)
    {
        var id = data[0]
        var status = data[1]
        var link = data[2]
        var class_remove = "state unpublish";
        var class_add   = "state publish";
        if(status == 0)
        {
            class_remove = "state publish"
            class_add   = "state unpublish";
        }
        $('a#confirm-'+id + ' span').removeClass(class_remove).addClass(class_add)
        $('a#confirm-'+id).attr('href',link)
    },"json")
}

function change_special(url)
{
    $.get(url,function(data)
    {
        var id = data[0]
        var status = data[1]
        var link = data[2]
        var class_remove = "state unpublish";
        var class_add   = "state publish";
        if(status == 0)
        {
            class_remove = "state publish"
            class_add   = "state unpublish";
        }
        $('a#special-'+id + ' span').removeClass(class_remove).addClass(class_add)
        $('a#special-'+id).attr('href','javascript:change_special(\' '+ link +'\')')
    },"json")
}

function change_group_acp_status(url)
{
    $.get(url,function(data)
    {
        var id = data[0]
        var group_acp = data[1]
        var link = data[2]
        var class_remove = "state unpublish";
        var class_add   = "state publish";
        if(group_acp % 2 == 0)
        {
            class_remove = "state publish"
            class_add   = "state unpublish";
        }
        $('a#group-acp-'+id + ' span').removeClass(class_remove).addClass(class_add)
        $('a#group-acp-'+id).attr('href','javascript:change_group_acp_status(\' '+ link +'\')')
    },"json")
}

function submit_form_(url)
{
    $('#adminForm').attr('action',url);
    $('#adminForm').submit();
}

function sort_link(objectHTML,column,order)
{
    $('input[name=filter_column]').val(column)
    $('input[name=filter_column_dir]').val(order)
    $('#adminForm').submit();
}

function change_pagination(page)
{
    $('input[name=filter_page]').val(page)
    $('#adminForm').submit();
}

$(document).ready(function()
{
    $('input[name=checkall-toggle]').change(function()
    {
        var check_status = this.checked
        $('#adminForm').find(':checkbox').each(function()
        {
            this.checked = check_status
        })
    })

    $('#filter-bar button[name=filter_submit]').click(function()
    {
        $('#adminForm').submit();
    })

    $('#filter-bar button[name=filter_clear]').click(function()
    {
        $('input[name=filter_search]').val("");
        $('#adminForm').submit();
    })

    $('#filter-bar select[name=filter_state]').change(function()
    {
        $('#adminForm').submit();
    })

    $('#filter-bar select[name=filter_cancel]').change(function()
    {
        $('#adminForm').submit();
    })

    $('#filter-bar select[name=filter_confirm]').change(function()
    {
        $('#adminForm').submit();
    })

    $('#filter-bar select[name=filter_special]').change(function()
    {
        $('#adminForm').submit();
    })

    $('#filter-bar select[name=filter_group_acp]').change(function()
    {
        $('#adminForm').submit();
    })

    $('#filter-bar select[name=filter_group_id]').change(function()
    {
        $('#adminForm').submit();
    })
})

