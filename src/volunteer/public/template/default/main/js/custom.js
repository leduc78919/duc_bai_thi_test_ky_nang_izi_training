function get_url_var(key)
{
    var result = new RegExp(key + "=([^&]*)","i").exec(window.location.search);
    return result && unescape(result[1]) || "";
}

function verify(link)
{
    $.get(link,function(data)
    {
        $('#adminForm').submit();
    })
}

$(document).ready(function()
{
    var controller = (get_url_var('controller'))?get_url_var('controller'):"index";
    var action     =  (get_url_var('action'))?get_url_var('action'):"index";
    var class_select  = controller +"-"+ action;
    console.log(class_select);
    $('#menu ul li.'+ class_select).addClass("selected");
    $('a.tab1').click(function()
    {
        $('div#tab1').css('display','block');
        $('div#tab2').css('display','none');
        $('a.tab1').addClass('active');
        $('a.tab2').removeClass('active');
    })
    $('a.tab2').click(function()
    {
        $('div#tab2').css('display','block');
        $('div#tab1').css('display','none');
        $('a.tab2').addClass('active');
        $('a.tab1').removeClass('active');
    })

    $('button[name=filter_clear]').click(function()
    {
        $('input[name=filter_search]').val("");
        $('#adminForm').submit();
    })
});

function change_pagination(page)
{
    $('input[name=filter_page]').val(page)
    $('#adminForm').submit();
}

function submit_form(link)
{
    $('#adminForm').attr("action",link);
    $('#adminForm').submit();
}

function change_quantity(link)
{
    $.get(link,function(data)
    {
        window.location.reload();
    })
}