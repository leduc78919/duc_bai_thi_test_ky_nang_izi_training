<?php
class bootstrap
{
   private $params;
   private $array_params;
   private $module;
   private $controller;
   private $action;
   private $controller_object;
   private $controller_name;
   private $file_path;
   public function init()
   {
      $this->setting();
      $this->load_file();

   }

   public function setting()
   {
      $this->params = array_merge($_GET,$_POST);
      if(!empty($this->params) && isset($this->params["module"]) && isset($this->params["controller"]) && isset($this->params["action"]))
      {
         $this->module     = $this->params["module"];
         $this->controller = $this->params["controller"];
         $this->action     = $this->params["action"];

      }
      else
      {
         $this->module     = DEFAULT_MODULE;
         $this->controller = DEFAULT_CONTROLLER;
         $this->action     = DEFAULT_ACTION;

         $this->params["module"] = DEFAULT_MODULE;
         $this->params["controller"] = DEFAULT_CONTROLLER;
         $this->params["action"] = DEFAULT_ACTION;
      }
      $this->array_params = $this->params;
      $this->controller_name = $this->controller."_controller";
      $this->file_path = MODULE_PATH."$this->module/controllers/".$this->controller_name . ".php";
   }
   public function load_file()
   {

      if(file_exists($this->file_path))
      {
         require_once($this->file_path);
         $this->controller_object = new $this->controller_name();
         $this->controller_object->init($this->array_params);

         $action_name = $this->action."_action";
         if(method_exists($this->controller_object,$action_name))
         {
            // echo "<pre>";
            // print_r($_SESSION);
            // echo "</pre>";
            if($this->module == "default")
            {
               $page_login = (($this->controller == "user"))?1:0;
               $logged = (isset($_SESSION["info"]["logged"]) && ($_SESSION["info"]["logged"] == true) && ($_SESSION["info"]["time"] + TIME_LOGIN > time()))?1:0;
               if($logged == 1)
               {
                  $this->controller_object->$action_name();
               }
               else
               {
                  if($page_login == 1)
                  {
                     helper::redirect("default","index","login");
                  }
                  else
                  {
                     $this->controller_object->$action_name();
                  }
               }
            }

         }
         else
         {
            helper::redirect("default","index","notice",array("type" => "not_url"));
         }
      }
      else
      {
         helper::redirect("default","index","notice",array("type" => "not_url"));
      }
   }

}