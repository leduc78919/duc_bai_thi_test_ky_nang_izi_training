<?php 
class Model_db
{
    public $conn;
    protected $db;
    protected $table;
    protected $resultQuery;
    public function __construct($arr = null)
    {
        if($arr == null)
        {
            $arr["host"] = DB_HOST;
            $arr["user"] = DB_USER;
            $arr["password"] = DB_PASSWORD;
            $arr["db"] = DB_NAME;
            $arr["table"] = DB_TABLE;

        }
        $conn = mysqli_connect($arr["host"],$arr["user"],$arr["password"]);
        if(!$conn) 
        {
            die("FAIL CONNNECT");
        }
        else
        {
            $this->conn = $conn;
            $this->db = $arr["db"];
            $this->table = $arr["table"];
            $this->select_db();
        }
    }

    // AFFECTED ROWS
    public function affected_rows()
    {
        return mysqli_affected_rows($this->conn);
    }

    // set connect
    public function set_conn($link)
    {
        $this->conn = $link;
    }
    // select database
    public function select_db($db = null)
    {
        if($db != null)
        {
            $this->db = $db;
        }
        mysqli_select_db($this->conn, $this->db);
    }
    // set table
    public function set_table($table)
    {
        $this->table = $table;
    }

    // insertQuery
    public function insert_query($data)
    {
        $newquerry = array();
        $cols ="";
        $rows = "";
        foreach($data as $key => $value)
        {
            $cols .= ", $key";
            $rows .= ", '$value'";
        }
        $newquerry['cols'] = substr($cols,2);
        $newquerry['rows'] = substr($rows,2);
        return $newquerry;
    }

    // insert 
    public function insert_db($data, $type = "single")
    {
        if($type == "single")
        {
            $newIN = $this->insert_query($data);
            $insert = mysqli_query($this->conn,"INSERT INTO `".$this->table."`(".$newIN['cols'].") VALUES (".$newIN['rows'].")");
            if(!$insert)
            {
                echo mysqli_error($this->conn);
            }
        }
        else
        {
            foreach($data as $value)
                {
                    $newIN = $this->insert_query($value);
                    $insert = mysqli_query($this->conn,"INSERT INTO `".$this->table."` (".$newIN['cols'] . ") VALUES (".$newIN['rows'].")");
                    if(!$insert)
                    {
                    die( mysqli_error($this->conn));
                    }
                }
        }
    }

    // random name
    
    public function random_name($number = 5)
    {   
        $array = array_merge(range('A','Z'),range('a','z'),range(0,9));
        $character = implode("",$array);
        $character = str_shuffle($character);    
        $character = substr($character,0,$number); 
        return $character;   
    }
    // fake data group
    public function fake_data($numberLoop)
    {
        $arr = array();
        for ($i = 1; $i < $numberLoop; $i++)
        {
            $sta = ($i % 2 == 0)?1:0;
            $arr[] = array("id" => $i, "name" => $this->random_name(7) ,"status" => $sta, "ordering" => $this->random_name(8));
        }
        return $arr;
    }

    // fake data user
    public function fake_data_user($numberLoop)
    {
        $arr = array();
        for ($i = 1; $i < $numberLoop; $i++)
        {
            $sta = ($i % 2 == 0)?1:0;
            $arr[] = array("id" => $i, "firstName" => $this->random_name(5), "lastName" => $this->random_name(5) ,"userName" => $this->random_name(8), 
                            "email" => $this->random_name(8), "password" => serialize($this->random_name(8)), "address" => $this->random_name(10), 
                            "status" => $sta, "ordering" => $i * 28, "group_id" => $i);
        }
        return $arr;
    }
    // close connect
    public function __destruct()
    {
        mysqli_close($this->conn);
    }

    // update query
    public function update_query($data)
    {
        $newquerry = "";
        foreach($data as $key => $value)
        {
            $newquerry .= ", $key = '$value'";
        }
        $newquerry = substr($newquerry,2);
        return $newquerry;
    }
    // update database
    public function update_db($data,$where)
    {
        $str = $this->update_query($data);
        $query = "UPDATE `".$this->table."` SET $str WHERE ".$this->where_query($where)."";
        $result = mysqli_query($this->conn,$query);
        if(!$result)
        {
            die(mysqli_error($this->conn));
        }
        return mysqli_affected_rows($this->conn);
    }

    // update where query
    public function where_query($where)
    {
        $newWhere = array();
        foreach($where as $value)
        {
            /* echo "<pre>";
            print_r($value);
            echo "</pre>"; */
            $newWhere[] = "`$value[0]` = '$value[1]'";
            if(isset($value[2]))
            {
                $newWhere[] = $value[2];
            }
        }
        $newStr = implode(" ",$newWhere);
        return $newStr;
    }

    // delete query
    public function delete_query($arr)
    {
        $newquerry = array();
        foreach($arr as $value)
        {
            $newquerry[] .= "`$value[0]` = '$value[1]'";
            if(isset($value[2]))
            {
                $newquerry[] = $value[2];
            }
        }
        $newquerry = implode(" ", $newquerry);
        return $newquerry;
    }
    //delete DB
    public function delete_db($array, $type = "single")
    {
        if($type == "single")
        {
            $str = $this->delete_query($array);
            $query = "DELETE FROM `".$this->table."` WHERE $str";
            $result = mysqli_query($this->conn,$query);
            if(!$result) 
            {
            die("fail remove");
            }
            return mysqli_affected_rows($this->conn);
        }
        else
        {

        }
    }

    //query
    public function query($query)
    {
        if(is_array($query))
        {
            $query = implode(" ", $query);
        }
        $this->resultQuery = mysqli_query($this->conn,$query);
        return $this->resultQuery;
    }
    // list record
    public function list_record($query)
    {
        $resultQuery = array();
        $result = $this->query($query);
        if (!$result) {
            die(mysqli_error($this->conn));
        } else {
            while($rows = mysqli_fetch_assoc($result))
            {
                $resultQuery[] = $rows;
            }
            mysqli_free_result($result);
        }
        return $resultQuery;
    }
    public function fetch_pairs($query)
    {
        $result = $this->query($query);
        $arr = array();
        if (!$result) {
            die(mysqli_error($this->conn));
        } else {
            while($rows = mysqli_fetch_assoc($result))
            {
                $arr[$rows["id"]] = $rows["name"];
            }
            return $arr;
        }
    }
    // check duplicate
    public function check_duplicate($value, $query, $checkDup)
    {
        $result = $this->single_record($query);
        if($value == $result[$checkDup])
        {
            return 1;
        }
        return 0;
    }

    //single record
    public function single_record($query)
    {
        $newRe = array();
        if(is_array($query))
        {
            $query = implode(" ", $query);
        }
        $result = $this->query($query);
        if (!$result) {
            die(mysqli_error($this->conn));
        }
        while($rows = mysqli_fetch_assoc($result))
        {
            $newRe = $rows;
        }
        mysqli_free_result($result);
        return $newRe;
    }

    public function exists_user_online($arrCheck)
    {
        $arr = $this->list_record($arrCheck);
        if(!empty($arr))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public function last_id()
    {
        $query[] = "SELECT max(id) as last_id";
        $query[] = 'FROM `'.$this->table.'`';
        $result = $this->single_record($query);
        return $result["last_id"];
    }

}