<?php 
require_once(TEMPLATE_SCRIPTS_PATH."php_thumb/ThumbLib.inc.php");
class upload
{
    public function __construct()
    {
        
    }

    function random_name($length)
    {
        $arr = array_merge(range("A","Z"),range("a","z"),range(0,9));
        $str = implode("",$arr);
        $str = str_shuffle($str);
        $str = substr($str,0,$length);
        return $str;
    }

    public function upload_file($file,$folder_upload,$options = null)
    { 
        if($file["tmp_name"] != null)
        {
            $extension = pathinfo($file["name"],PATHINFO_EXTENSION);
            $name = $this->random_name(5) .".".$extension;
            $file_dir = PUBLIC_FILE_PATH . $folder_upload."/".$name;
            copy($file["tmp_name"],$file_dir);
            $img = PhpThumbFactory::create($file_dir);
            $img->adaptiveResize(60,90);
            $img->save(PUBLIC_FILE_PATH . $folder_upload."/60x90-".$name);
            $name_resized = "60x90-".$name;
            return $name_resized;

        }  
    }
}