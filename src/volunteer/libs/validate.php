<?php
class validate {
    protected $error = array();
    protected $result = array();
    public function check_name($value)
    {
      if($value == null)
      {
        $this->error["name"] = "Ten khong duoc rong";
      }
      else if(strlen($value) < 4)
      {
        $this->error["name"] = "Do dai ten khong hop le";
      }
      else
      {
        $this->result["name"] = $value;
      }
    }

    public function check_gender($value)
    {
      if($value == 0)
      {
        $this->error["gender"] = "Xin moi chon gioi tinh";
      }
      else
      {
        $this->result["gender"] = $value;
      }
    }

    public function check_email($value)
    {
      if($value == null)
      {
        $this->error["email"] = "email khong duoc rong";
      }
      else if(!filter_var($value,FILTER_VALIDATE_EMAIL))
      {
        $this->error["email"] = "email khong hop le";
      }
      else
      {
        $this->result["email"] = $value;
      }
    }

    public function check_phone($value)
    {
      if($value == null)
      {
        $this->error["phone"] = "phone khong duoc rong";
      }
      else if(!filter_var($_POST["phone"],FILTER_VALIDATE_INT))
      {
        $this->error["phone"] = "So dien thoai khong hop le";
      }
      else
      {
        $this->result["phone"] = $value;
      }

    }

    public function check_birthday($value)
    {
      if($value == null)
      {
        $this->error["birthday"] = "ngay sinh khong duoc rong";
      }
      else
      {
        $this->result["birthday"] = $value;
      }

    }

    public function check_so_luong($value)
    {
      if($value == null)
      {
        $this->error["number"] = "so luong khong duoc rong";
      }
      else if($value < 5 || $value > 100)
      {
        $this->error["number"] = "so luong khong phu hop";
      }
      else
      {
        $this->result["number"] = $value;
      }
    }

    public function check_time($value,$compare,$options)
    {
      switch($options["tag"])
      {
        case "min":
          {
            if($value > $compare)
            {
              $this->error["notime"][$options["tag"]] = "Thoi gian khong phu hop";
            }
            else
            {
              $this->result["notime"][$options["tag"]] = $value;
            }
            break;
          }
        case "max":
          {
            if($value < $compare)
              {
                $this->error["notime"][$options["tag"]] = "Thoi gian khong phu hop";
              }
              else
              {
                $this->result["notime"][$options["tag"]] = $value;
              }
              break;
          }
          case "between":
            {
              if($value > $compare)
              {
                $this->error["notime"][$options["tag"]] = "Thoi gian khong phu hop";
              }
              else
              {
                $this->result["notime"][$options["tag"]] = $value;
              }
              break;
           }
        }
        if($value == null)
        {
          $this->error["notime"][$options["tag"]] = "Thoi gian khong duoc rong";
        }
    }

    public function check_description($value)
    {
      if($value == null)
      {
        $this->error["desc"] = "phan mo ta khong duoc rong";
      }
      else if(strlen($value) < 10)
      {
        $this->error["desc"] = "phan mo ta phai lon hon 10";
      }
      else
      {
        $this->result["desc"] = $value;
      }
    }

    public function check_number($value,$options = null)
    {
      if($options["tag"] == "min")
      {
        if($value == null)
        {
          $this->error["min"] = "so luong khong duoc rong";
        }
        else if($value < $options["condition"])
        {
          $this->error["min"] = "so luong phai lon hon ". $options["condition"];
        }
        else
        {
          $this->result["min"] = $value;
        }
      }
      else if($options["tag"] == "max")
      {
        if($value == null)
        {
          $this->error["max"] = "so luong khong duoc rong";
        }
        else if($value > $options["condition"])
        {
          $this->error["max"] = "so luong phai be hon ". $options["condition"];
        }
        else
        {
          $this->result["max"] = $value;
        }
      }
    }

    public function show_error()
    {
      return $this->error;
    }

    public function show_result()
    {
      return $this->result;
    }
}