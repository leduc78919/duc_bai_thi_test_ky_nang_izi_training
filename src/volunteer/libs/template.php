<?php 
class template
{
    private $_file_config;
    private $_file_template;
    private $_folder_template;
    private $_controller;

    public function __construct($controller)
    {
        $this->_controller = $controller;
    }

    public function set_all($file_path, $file_name, $file_config)
    {
        $this->set_folder_template($file_path);
        $this->set_file_template($file_name);
        $this->set_file_config($file_config);
        $this->load();
    }
    public function load()
    {
        $file_config = $this->get_file_config();
        $file_template = $this->get_file_template();
        $folder_template = $this->get_folder_template();

        $template_path = TEMPLATE_PATH. $folder_template . "/". $file_template;
        $template_config_path = TEMPLATE_PATH. $folder_template . "/". $file_config;
        if(file_exists($template_path))
        {
            $this->_controller->view->set_template($template_path);
        }
        if(file_exists($template_config_path))
        {
            $arr_config = parse_ini_file($template_config_path);
            $this->_controller->view->set_title($this->create_title($arr_config["title"]));
            $this->_controller->view->set_meta_http($this->create_meta_http($arr_config["meta_http"]));
            $this->_controller->view->set_meta_name($this->create_meta_name($arr_config["meta_name"]));
            $this->_controller->view->set_css($this->create_link_css($arr_config["dir_css"],$arr_config["file_css"]));
            $this->_controller->view->set_js($this->create_link_js($arr_config["dir_js"],$arr_config["file_js"]));
            $this->_controller->view->set_images($arr_config["dir_img"]);

        }
        
    }
    public function create_title($value)
    {
        return '<title>'.$value.'</title>';
    }

    public function create_meta_http($value)
    {
        if(!empty($value))
        {
            $meta_http = "";
            foreach($value as $value_http)
            {
                $temp = explode('|',$value_http);
                $meta_http .= '<meta http-equiv="'.$temp[0].'" content="'.$temp[1].'">';
            }
            return $meta_http;
        }
    }
    public function create_meta_name($value)
    { 
        $meta_name = "";
        if(!empty($value))
        {
           
            foreach($value as $value_http)
            {
                $temp = explode('|',$value_http);
                $meta_name .= '<meta name="'.$temp[0].'" content="'.$temp[1].'">';
            }
           
        } 
        return $meta_name;
    }

    public function create_link_css($dir_css, $name_css)
    {
        $dir_path_css = TEMPLATE_LINK_PATH.$this->_folder_template."/".$dir_css."/";
        $css_path = "";
        if(!empty($name_css))
        {
            foreach($name_css as $value)
            {
                $css_path .= '<link rel="stylesheet" type="text/css" href="'.$dir_path_css.$value.'"/>';
            }
        }
        return $css_path;
    }

    public function create_link_js($dir_js, $name_js)
    {
        $dir_path_js = TEMPLATE_LINK_PATH.$this->_folder_template."/".$dir_js."/";
        $js_path = "";
        if(!empty($name_js))
        {
            foreach($name_js as $value)
            {
                $js_path .= '<script type="text/javascript" src="'.$dir_path_js.$value.'"></script>';
            }
        }
        return $js_path;
    }

    public function set_file_template($value = "index.php")
    {
        $this->_file_template = $value;
    }
    public function get_file_template()
    {
        return $this->_file_template;
    }

    public function set_file_config($value = "template.ini")
    {
        $this->_file_config = $value;
    }
    public function get_file_config()
    {
        return $this->_file_config;
    }

    public function set_folder_template($value = "default/main")
    {
        $this->_folder_template = $value;
    }
    public function get_folder_template()
    {
        return $this->_folder_template;
    }
}