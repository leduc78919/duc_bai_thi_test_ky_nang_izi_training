<?php 
class pagination {
    private $total_items; // tong so phan tu 
    private $items_per_page; // item per page ( so phan tu moi trang )
    private $max_page; // tong so trang
    private $current_page; // trang hien tai
    private $page_range; // so trang hien thi

    public function __construct($total_items, $pagination)
    {
        $this->total_items = $total_items;
        $this->items_per_page = $pagination["items_per_page"];
        $max_page = ceil($total_items/$pagination["items_per_page"]);
        $this->max_page = $max_page;
        $this->current_page = $pagination["current_page"];
        if($pagination["page_range"] % 2 == 0)
        {
            $pagination["page_range"] += 1;
        }
        $this->page_range = $pagination["page_range"];
    }
    
    public function create_html($link)
    {
        $start = ' <div class="button2-right off">
                        <div class="start"><span>Start</span></div>
                    </div>';
        $previous = '<div class="button2-right off">
                        <div class="prev"><span>Prev</span></div>
                    </div>';
        if($this->current_page > 1)
        {
            $start     = '<div class="button2-right">
                                <div class="start"><a onclick = "javascript:change_pagination(1)" href="#">START</a></div>
                            </div>';
            $previous  = '<div class="button2-right">
                            <div class="prev"><a onclick = "javascript:change_pagination('.($this->current_page-1).')" href="#">PREVIOUS</a></div>
                        </div>';
        }
        $next = '<div class="button2-left off">
                    <div class="next"><span>NEXT</span></div>
                </div>';
        $end = '<div class="button2-left off">
                    <div class="next"><span>END</span></div>
                </div>';
        if($this->current_page < $this->max_page)
        {
            $next     = '<div class="button2-left">
                            <div class="next"><a  onclick = "javascript:change_pagination('.($this->current_page+1).')" href="#">NEXT</a></div>
                        </div>';
            $end      = '<div class="button2-left">
                            <div class="next"><a onclick = "javascript:change_pagination('.$this->max_page.')"  href="#">END</a></div>
                        </div>';
        }

        // list page
        $list_page = '';
        if($this->page_range < $this->max_page)
        {
            if($this->current_page == 1)
            {
                $start_page = 1;
                $end_page =$this->page_range;
            }
            else if($this->current_page == $this->max_page)
            {
                $start_page = $this->max_page - $this->page_range + 1;
                $end_page = $this->max_page;
            }
            else
            {
                $start_page = $this->current_page - ($this->page_range -1)/2;
                $end_page = $this->current_page + ($this->page_range - 1)/2;
            }
            if($start_page < 1)
            {
                $end_page = $end_page + (1-$start_page);
                $start_page = 1;
            }
            if($end_page > $this->max_page)
            {
                $start_page =  $this->max_page - $this->page_range + 1;;
                $end_page = $this->max_page;
            }
        }
        else
        {
            $start_page = 1;
            $end_page =$this->max_page;
        }

        for($i = $start_page; $i <= $end_page; $i++)
        {
            if($i == $this->current_page)
            {
                $list_page .= '<div class="button2-left">
                                    <div class="page"><span>'.$i.'</span></div>
                                </div>';
            }
            else
            {
                $list_page .= '<div class="button2-left off">
                                    <div class="page"><a onclick = "javascript:change_pagination('.$i.')"  href="#">'.$i.'</a></div>
                                </div>';
            }

        }

        $pagination = '<div class="pagination">
                            '.$start.$previous . $list_page . $next. $end.'
                        <div class="limit">Page '.$this->current_page.' of '.$this->max_page.'</div>
                        <input type="hidden" name="limitstart" value="0">
                                        </div>';
        return $pagination;

    }

}