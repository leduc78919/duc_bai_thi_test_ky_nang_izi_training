<?php
class user_model extends Model_db
{
  public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_HOATDONG;
    }

    public function save_item($result,$options = null)
    {
        if($options == null)
        {
            $query[] = 'INSERT INTO `'.$this->table.'`(TenHD,MoTaHD,NgayGioBD,NgayGioKT,SLToiThieuYC,SLToiDaYC,ThoiHanDK,TrangThai,MaTV)';
            $query[] = 'VALUES("'.$result["name"].'","'.$result["desc"].'","'.$result["notime"]["min"].'",
                                "'.$result["notime"]["max"].'",'.$result["min"].','.$result["max"].',
                                "'.$result["notime"]["between"].'",'.$result["state"].','.$result["MaTV"].')';
            $this->query($query);
        }
        else if($options["tag"] == "participate")
        {
            $query[] = 'INSERT INTO `'.DB_TABLE_THAMGIA.'`(MaTV,MaHD,NgayGioDangKy)';
            $query[] = 'VALUES('.$_SESSION["info"]["id"]["MaTV"].','.$result["MaHD"].',"'.date("Y/m/d H:i:s",time()).'")';
            $this->query($query);
        }
        else if($options["tag"] == "edit")
        {
            $query[] = 'UPDATE `'.DB_TABLE_HOATDONG.'`';
            $query[] = 'SET TenHD = "'.$result["nameAct"].'",MoTaHD = "'.$result["description"].'",
                    NgayGioBD = "'.$result["start"].'",NgayGioKT = "'.$result["end"].'", SLToiThieuYC = '.$result["numberMin"].'
                    , SLToiDaYC = '.$result["numberMax"].',ThoiHanDK = "'.$result["time"].'"';
            $query[] = 'WHERE MaHD = '.$result["id"].' AND MaTV = '. $_SESSION["info"]["id"]["MaTV"];
            $this->query($query);
        }
    }

    public function get_items($params, $options = null)
    {
        $query[] = 'SELECT *';
        $query[] = 'FROM `'.DB_TABLE_HOATDONG.'`';
        $query[] = 'WHERE MaTV = '. $_SESSION["info"]["id"]["MaTV"];
        $result = $this->list_record($query);
        return $result;
    }

    public function get_single_item($params,$options = null)
    {
        if($options["tag"] == "user_edit")
        {
            $query[] = 'SELECT MaHD,TenHD as name, MoTaHD as desct, SLToiThieuYC as min, SLToiDaYC as max,NgayGioBD,NgayGioKT,ThoiHanDK';
            $query[] = 'FROM `'.DB_TABLE_HOATDONG.'`';
            $query[] = 'WHERE MaHD = '.$params["MaHD"].' AND MaTV = '.$_SESSION["info"]["id"]["MaTV"].'';
            $query[] = 'ORDER BY MaHD DESC';
            $result = $this->single_record($query);
            return $result;
        }
    }

    public function delete_item($params,$options = null)
    {
        $query = 'DELETE FROM `'.DB_TABLE_HOATDONG.'` WHERE MaHD = '.$params["MaHD"];
        $this->query($query);
    }

    public function check_users($params,$options = null)
    {
        if($options["tag"] == "save")
        {
            $query = 'SELECT MaTV FROM `'.DB_TABLE_THAMGIA.'` WHERE MaHD = '.$params["MaHD"];
            $query_re = mysqli_query($this->conn,$query);
            $result = array();
            while($rows = mysqli_fetch_assoc($query_re))
            {
                $result[] = $rows["MaTV"];
            }
            return $result;
        }
    }

}

