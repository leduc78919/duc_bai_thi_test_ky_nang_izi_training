<?php

use Egulias\EmailValidator\Validation\Exception\EmptyValidationList;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

class index_model extends Model_db
{

    public function __construct()
    {
        parent::__construct();
        $this->table = DB_TABLE_THANHVIEN;
    }

    public function save_item($result)
    {
        $query[] = 'INSERT INTO `'.$this->table.'`(HoTen,GioiTinh,NgaySinh,DiaChiEmail,SoDienThoai)';
        $query[] = 'VALUES("'.$result["name"].'",'.$result["gender"].',"'.$result["birthday"].'","'.$result["email"].'",'.$result["phone"].')';
        $this->query($query);
    }

    public function getMaTV($params)
    {
        $query[] = 'SELECT MaTV FROM `THANHVIEN` WHERE DiaChiEmail = "'.$params["email"].'" AND SoDienThoai = '.$params["phone"].'';
        $result = $this->single_record($query);
        return $result;
    }

    public function get_all_items()
    {
        $query[] = 'SELECT HD.MaTV,HD.MaHD,HD.TenHD,HD.MoTaHD,HD.NgayGioBD,HD.NgayGioKT,HD.SLToiThieuYC,HD.SLToiDaYC,HD.ThoiHanDK,HD.TrangThai,TV.Hoten ';
        $query[] = 'FROM `'.DB_TABLE_THANHVIEN.'` as TV, `'.DB_TABLE_HOATDONG.'` as HD';
        $query[] = 'WHERE TV.MaTV = HD.MaTV';
        $query[] = 'ORDER BY HD.MaHD DESC';
        $result = $this->list_record($query);
        return $result;
    }

    public function get_average()
    {
        $list_sum = $this->cal_sum();
        $list_info = $this->get_info_captain();
        $result = array();
        foreach($list_info as $key => $value)
        {
            if(!empty($result[$value["MaTV"]]))
            {
                $result[$value["MaTV"]]["DiemTruongDoan"] += $list_sum[$key]["DiemTruongDoan"];
                $result[$value["MaTV"]]["DiemTieuChi1"] += $list_sum[$key]["DiemTieuChi1"];
                $result[$value["MaTV"]]["DiemTieuChi2"] += $list_sum[$key]["DiemTieuChi2"];
                $result[$value["MaTV"]]["DiemTieuChi3"] += $list_sum[$key]["DiemTieuChi3"];

                $result[$value["MaTV"]]["SLDiemTruongDoan"] += $list_sum[$key]["SL0"];

            }
            else
            {
                $result[$value["MaTV"]]["HoTen"] = $value["HoTen"];
                $result[$value["MaTV"]]["DiemTruongDoan"] = $list_sum[$key]["DiemTruongDoan"];
                $result[$value["MaTV"]]["DiemTieuChi1"] = $list_sum[$key]["DiemTieuChi1"];
                $result[$value["MaTV"]]["DiemTieuChi2"] = $list_sum[$key]["DiemTieuChi2"];
                $result[$value["MaTV"]]["DiemTieuChi3"] = $list_sum[$key]["DiemTieuChi3"];

                $result[$value["MaTV"]]["SLDiemTruongDoan"] = $list_sum[$key]["SL0"];

            }
        }
       return $result;



    }

    public function cal_sum()
    {
        $query[] = 'SELECT MaHD,SUM(DiemTruongDoan) as d0, SUM(DiemTieuChi1) as d1,SUM(DiemTieuChi2) as d2,SUM(DiemTieuChi3) as d3,';
        $query[] = 'COUNT(DiemTruongDoan) as n0';
        $query[] = 'FROM `'.DB_TABLE_THAMGIA.'`';
        $query[] = 'GROUP BY (MaHD)';
        $query = implode(" ",$query);
        $result_que = mysqli_query($this->conn,$query);
        while($rows = mysqli_fetch_assoc($result_que))
        {
            $result[$rows["MaHD"]] = array("DiemTruongDoan" => $rows["d0"],
                                            "DiemTieuChi1" => $rows["d1"],
                                            "DiemTieuChi2" => $rows["d2"],
                                            "DiemTieuChi3" => $rows["d3"],
                                            "SL0" => $rows["n0"],
                                          );
        }
        return $result;
    }
    public function get_info_captain($id = null)
    {
        $result = array();
        $query[] = 'SELECT tv.MaTV,tv.HoTen,hd.MaHD';
        $query[] = 'FROM `'.DB_TABLE_HOATDONG.'` as hd INNER JOIN `'.DB_TABLE_THANHVIEN.'` as tv ON hd.MaTV = tv.MaTV';
        $query = implode(" ",$query);
        $result_que = mysqli_query($this->conn,$query);
        while($rows = mysqli_fetch_assoc($result_que))
        {
            $result[$rows["MaHD"]] = array("MaTV" => $rows["MaTV"],"HoTen" => $rows["HoTen"]);
        }
        return $result;
    }


}