<?php
class user_controller extends controller
{
  public function create_act_action()
  {
    // echo "<pre>";
    // print_r($this->params);
    // echo "</pre>";
    if(isset($this->params["token"]))
    {
      $name = $this->params["nameAct"];
      $desc = $this->params["description"];
      $start = strtotime($this->params["start"]);
      $end = strtotime($this->params["end"]);
      $min = $this->params["numberMin"];
      $max = $this->params["numberMax"];
      $time = strtotime($this->params["time"]);

      $validate = new validate();
      $validate->check_name($name);
      $validate->check_description($desc);
      $validate->check_time($start,$end,array("tag" => "min"));
      $validate->check_time($end,$start,array("tag" => "max"));
      $validate->check_time($time,$start,array("tag" => "between"));
      $validate->check_number($min,array("tag" => "min", "condition" => 4));
      $validate->check_number($max,array("tag" => "max", "condition" => 100));
      $this->view->error = $validate->show_error();
      $this->view->result = $validate->show_result();
      if(empty($this->view->error))
      {
        $this->view->result["state"] = "1";
        $this->view->result["MaTV"] = $_SESSION["info"]["id"]["MaTV"];
        $this->view->result["notime"]["min"] = date("Y/m/d",$this->view->result["notime"]["min"]);
        $this->view->result["notime"]["max"] = date("Y/m/d",$this->view->result["notime"]["max"]);
        $this->view->result["notime"]["between"] = date("Y/m/d",$this->view->result["notime"]["between"]);
        $this->db->save_item($this->view->result);
        Session::set("success","Da tao hoat dong thanh cong");
        helper::redirect("default","index","index");
      }
      else
      {
        $this->view->render("user/create_act");
      }

    }

    $this->view->render("user/create_act");
  }

  public function participate_action()
  {
    $list_user = $this->check_exists_user($this->params);
    if(!in_array($_SESSION["info"]["id"]["MaTV"],$list_user))
    {
      $this->db->save_item($this->params,array("tag" => "participate"));
      Session::set("participateSuccess",$this->params["MaHD"]);
      helper::redirect("default","index","index");
    }
    else
    {
      Session::set("flag_participate",1);
      helper::redirect("default","index","index");
    }


  }

  public function check_exists_user()
  {
    return $this->db->check_users($this->params,array("tag" => "save"));
  }

  public function list_edit_action()
  {
    $this->view->items = $this->db->get_items($this->params,array("tag" => "edit"));
    $this->view->render("user/list_edit");
  }

  public function detail_activity_action()
  {
    $this->view->result = $this->db->get_single_item($this->params,array("tag" => "user_edit"));
    $this->view->render("user/detail_activity");
  }

  public function edit_detail_action()
  {
    $this->params["start"] = date("Y/m/d",strtotime($this->params["start"]));
    $this->params["end"] = date("Y/m/d",strtotime($this->params["end"]));
    $this->params["time"] = date("Y/m/d",strtotime($this->params["time"]));
    $this->db->save_item($this->params,array("tag" => "edit"));
    Session::set("success","Edit thanh cong");
    helper::redirect("default","user","list_edit");
  }

  public function delete_activity_action()
  {
    if($this->params["state"] != "Da ket thuc")
    {
      $this->db->delete_item($this->params);
      Session::set("success","Da xoa hoat dong thanh cong");
      helper::redirect("default","user","list_edit");

    }
    else
    {
      Session::set("success","Khong the xoa hoat dong");
      helper::redirect("default","user","list_edit");
    }
  }

  public function mark_action()
  {

  }
}