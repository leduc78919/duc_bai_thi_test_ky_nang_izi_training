<?php
class index_controller extends controller
{
    public function index_action()
    {
        $this->view->result = $this->db->get_all_items();
        $this->view->render("index/index");
    }

    public function register_action()
    {
        // echo "<pre>";
        // print_r($this->params);
        // echo "</pre>";
        $result = array();
        $error = array();
        if(isset($_POST["token"]))
        {
            $name = $_POST["name"];
            $gender = $_POST["gender"];
            $birthday = $_POST["birthday"];
            $email = $_POST["email"];
            $phone = $_POST["phone"];

            $validate = new validate();
            $validate->check_name($name);
            $validate->check_email($email);
            $validate->check_gender($gender);
            $validate->check_phone($phone);
            $validate->check_birthday($birthday);
            $error = $validate->show_error();
            $result = $validate->show_result();

            if(empty($error))
            {
                $result["birthday"] = date("Y/m/d",strtotime($result["birthday"]));
                $this->db->save_item($result);
                Session::set("success","dang ky thanh cong");
                helper::redirect("default","index","index");
            }
            else
            {
                $this->view->error = $error;
                $this->view->result = $result;
                $this->view->render("index/register");
            }
        }
        $this->view->render("index/register");
    }

    public function login_action()
    {
        // echo "<pre>";
        // print_r($this->params);
        // echo "</pre>";
        if(isset($this->params["token"]))
        {
        //Get data from table Thanh vien
        $email = $this->params["email"];
        $phone = $this->params["phone"];
        $validate = new validate();
        $validate->check_email($email);
        $validate->check_phone($phone);
        $error = $validate->show_error();

        if(empty($error))
        {
            $maTV = $this->db->getMaTV($this->params);
            if(!empty($maTV))
            {
                $_SESSION["info"] = array(
                    "id" => $maTV,
                    "logged" => 1,
                    "time" => time()
                );
                helper::redirect("default","index","index");
            }
            else
            {
                Session::set("error","thong tin dang nhap khong chinh xac");
                $this->view->render("index/login");
            }
        }
        else
        {
            $this->view->error = $error;
            $this->view->render("index/login");
        }
        }
        $this->view->render("index/login");
    }

    public function logout_action()
    {
        Session::delete("info");
        Session::delete("flag_participate");
        helper::redirect("default","index","index");
    }

    public function average_action()
    {
        $this->view->result = $this->db->get_average();
        $this->view->render("index/average");
    }
}