<?php
    $link = url::create_url("default","index","login");
    if(isset($_SESSION["error"]))
    {
        $message = $_SESSION["error"];
        Session::delete("error");
    }
?>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../assets/style.css">
  <title>Document</title>
</head>
<body>
  <form action="<?php echo $link; ?>" method="POST">
    <h3> DANG NHAP THANH VIEN</h3>
    <h4 style="color: red;"> <?php echo @$message;?></h4>
    <div style="margin-bottom: 20px;">
      <label>Dia chi email</label>
      <input type="text" name="email">
      <p style="color: red;"><?php echo @$this->error["email"] ?></p>
    </div>
    <div style="margin-bottom: 20px;">
      <label>So dien thoai</label>
      <input type="text" name="phone">
      <p style="color: red;"><?php echo @$this->error["phone"] ?></p>
    </div>
    <input type="hidden" name="token" value="<?php echo time();?>">
    <input type="submit" name="submit">
  </form>
</body>
</html>