<?php
    $link =  url::create_url("default","index","register");
?>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CREATE NEW ACTIVITY</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
</head>
<body>
  <form action="<?php echo $link; ?>" method="post">
    <div>
        <p>Ho ten</p>
        <input type="text" id="name" name="name" value="<?php echo @$this->result["name"] ?>">
        <p style="color: red;"><?php echo @$this->error["name"] ?></p>
    </div>
    <div style="margin-top: 20px;">
        <select name="gender" id="" >
          <option value="0" > -- select gender -- </option>
          <option value="1">nu</option>
          <option value="2">nam</option>
        </select>
        <p style="color: red;"><?php echo @$this->error["gender"] ?></p>
    </div>
    <div>
       <p > Ngay Sinh</p>
    <input type="text" name="birthday" id="datepicker" value="<?php echo @$this->result["birthday"] ?>">
    <p style="color: red;"><?php echo @$this->error["birthday"] ?></p>
    </div>
    <div>
      <p>Dia chi email</p>
    <input type="text" name="email" id="email" value="<?php echo @$this->result["email"] ?>">
    <p style="color: red;"><?php echo @$this->error["email"] ?></p>
    </div>
    <div>
      <p>So dien thoai(+84) </p>
    <input type="text" name="phone" id="phone" value="<?php echo @$this->result["phone"] ?>">
    <p style="color: red;"><?php echo @$this->error["phone"] ?></p>
    </div>
    <input type="hidden" name="token" value="<?php echo time() ?>">
    <div style="margin-top: 20px;">
      <input type="submit" name="submit" >
    </div>


  </form>
</body>
</html>