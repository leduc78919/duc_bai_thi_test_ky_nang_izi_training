<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INDEX</title>
</head>
<body>
<div>
  <?php
    if(isset($_SESSION["info"]["logged"]))
    {
      echo '<p><a href="index.php?module=default&controller=user&action=create_act">Create activity</a></p>';
      echo '<p><a href="index.php?module=default&controller=index&action=logout">Logout</a></p>';
      echo '<p><a href="index.php?module=default&controller=user&action=list_edit">Edit activities</a></p>';
      echo '<p><a href="index.php?module=default&controller=user&action=mark">Mark activities</a></p>';
      echo '<p><a href="index.php?module=default&controller=index&action=average">Thong ke diem trung binh</a></p>';

    }
    else
    {
  ?>
    <p><a href="index.php?module=default&controller=index&action=register">Register</a></p>
    <p><a href="index.php?module=default&controller=index&action=login">Login</a></p>
    <p><a href="index.php?module=default&controller=index&action=average">Thong ke diem trung binh</a></p>
  <?php
    }
      if(isset($_SESSION["success"]))
      {
        $message = $_SESSION["success"];
        Session::delete("success");
        echo "<h4>$message</h4>";
      }
    ?>
  </div>
  <div>
      <?php
      foreach($this->result as $value)
      {
        $link_register = url::create_url("default","user","participate",array("MaHD" => $value["MaHD"]));
        $currentDay = time();
        if(strtotime($value["ThoiHanDK"]) < $currentDay && $currentDay < strtotime($value["NgayGioKT"]))
        {
          $value["TrangThai"] = 2;
        }
        else if($currentDay > strtotime($value["NgayGioKT"]))
        {
          $value["TrangThai"] = 4;
        }
        switch($value["TrangThai"])
        {
          case 1:
            {
              $state = "Dang mo dang ky";
              break;
            }
          case 2:
            {
              $state = "Da het han dang ky";
              break;
            }
          case 3:
            {
              $state = "Truong doan tu huy";
              break;
            }
          case 4:
            {
              $state = "Da ket thuc";
              break;
            }
        }
        if(isset($_SESSION["info"]) && $value["MaTV"] == $_SESSION["info"]["id"]["MaTV"])
        {
          $link_E = url::create_url("default","user","detail_activity",array("MaHD" => $value["MaHD"]));
          $xhtml = '<p style="display: inline-block; background-color:greenyellow;padding:10px;"><a href="'.$link_E.'" >Edit</a></p>';
        }
        else if($state == "Dang mo dang ky")
        {
          if(isset($_SESSION["participateSuccess"]) || isset($_SESSION["flag_participate"]))
          {
            Session::delete("participateSuccess");
            $xhtml = '<p style="display: inline-block; background-color:greenyellow;padding:10px;">Dang ky thanh cong</p>';
          }
          else
          {
            $xhtml = '<p style="display: inline-block; background-color:aqua;padding:10px;"><a href="'.$link_register.'" >Dang ky Tham gia</a></p>';
          }
        }
        else
        {
          $xhtml = '<p style="display: inline-block; background-color:aqua;padding:10px;">Het han dang ky</p>';
        }
        echo '<div style="border:2px solid grey; width: 30%; margin: 10px; padding: 10px;">
                <p>Ten hoat dong: '.$value["TenHD"].'</p>
                <p>truong doan: '.$value["Hoten"].'</p>
                <p>Ngay bat dau: '.$value["NgayGioBD"].'</p>
                <p>Ngay ket thuc: '.$value["NgayGioKT"].'</p>
                <p>Thoi han dang ky: '.$value["ThoiHanDK"].'</p>
                <p>Trang thai:  '.$state.'</p>
                <p>So luong:  '.$value["SLToiDaYC"].'</p>
                '.$xhtml.'
              </div>';
      }
      ?>
  </div>
</body>
</html>