<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INDEX</title>
</head>
<body>
<p><a href="index.php?module=default&controller=index&action=index">Home</a></p>
<div>
  <?php
    if(isset($_SESSION["info"]["logged"]))
    {
      echo '<p><a href="index.php?module=default&controller=user&action=create_act">Create activity</a></p>';
      echo '<p><a href="index.php?module=default&controller=index&action=logout">Logout</a></p>';
    }
    else
    {
  ?>
    <p><a href="index.php?module=default&controller=index&action=register">Register</a></p>
    <p><a href="index.php?module=default&controller=index&action=login">Login</a></p>
  <?php
    }
      if(isset($_SESSION["success"]))
      {
        $message = $_SESSION["success"];
        Session::delete("success");
        echo "<h4>$message</h4>";
      }
    ?>
  </div>
  <div>
      <?php
      foreach($this->items as $value)
      {
        $currentDay = time();
        $link_edit = url::create_url("default","user","detail_activity",array("MaHD" => $value["MaHD"]));
        $state = helper::check_state($value["TrangThai"],$currentDay,$value["ThoiHanDK"],$value["NgayGioKT"]);
        $link_delete = url::create_url("default","user","delete_activity",array("MaHD" => $value["MaHD"], "state" => $state));
        if(strtotime($value["ThoiHanDK"]) < $currentDay && $currentDay < strtotime($value["NgayGioKT"]))
        {
          $value["TrangThai"] = 2;
        }
        else if($currentDay > strtotime($value["NgayGioKT"]))
        {
          $value["TrangThai"] = 4;
        }
        switch($value["TrangThai"])
        {
          case 1:
            {
              $state = "Dang mo dang ky";
              break;
            }
          case 2:
            {
              $state = "Da het han dang ky";
              break;
            }
          case 3:
            {
              $state = "Truong doan tu huy";
              break;
            }
          case 4:
            {
              $state = "Da ket thuc";
              break;
            }
        }
        $xhtml      = '<p style="display: inline-block; background-color:aqua;padding:10px;"><a href="'.$link_edit.'" >Edit</a></p>';
        $deletehtml = '<p style="display: inline-block; background-color:aqua;padding:10px;margin-left:20px;"><a href="'.$link_delete.'" >Delete</a></p>';
        echo '<div style="border:2px solid grey; width: 30%; margin: 10px; padding: 10px;">
                <p>Ten hoat dong: '.$value["TenHD"].'</p>
                <p>Ngay bat dau: '.$value["NgayGioBD"].'</p>
                <p>Ngay ket thuc: '.$value["NgayGioKT"].'</p>
                <p>Thoi han dang ky: '.$value["ThoiHanDK"].'</p>
                <p>Trang thai:  '.$state.'</p>
                <p>So luong:  '.$value["SLToiDaYC"].'</p>
                '.$xhtml. $deletehtml.'
              </div>';
      }
      ?>
  </div>
</body>
</html>