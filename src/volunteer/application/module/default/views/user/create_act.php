<?php
  $link = url::create_url("default","user","create_act");
  $start = (isset($this->result["notime"]["min"]))? date("d/m/Y",$this->result["notime"]["min"]):"";
  $end = (isset($this->result["notime"]["max"]))? date("d/m/Y",$this->result["notime"]["max"]):"";
  $time = (isset($this->result["notime"]["between"]))? date("d/m/Y",$this->result["notime"]["between"]):"";
?>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CREATE NEW ACTIVITY</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <link rel="stylesheet" href="../assets/style.css">
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
  <script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
  </script>
</head>
<body>
<p><a href="index.php?module=default&controller=index&action=index">Home</a></p>
  <form action="<?php echo $link; ?>" method="post">
    <div>
        <p>Ten Hoat Dong</p>
        <input type="text" id="nameAct" name="nameAct" value="<?php echo @$this->result["name"] ?>">
        <p style="color: red;"><?php echo @$this->error["name"] ?></p>
    </div>
    <div>
      <p for="">Mo ta hoat dong</p>
      <textarea name="description" id="description" cols="30" rows="10" value=""><?php echo @$this->result["desc"] ?></textarea>
      <p style="color: red;"><?php echo @$this->error["desc"] ?></p>
    </div>
    <div>
        <p >Ngay bat dau</p>
      <input type="text" name="start" class="datepicker" value="<?php echo $start?>">
      <p style="color: red;"><?php echo @$this->error["notime"]["min"] ?></p>
    </div>
    <div>
       <p > Ngay ket thuc</p>
    <input type="text" name="end" class="datepicker" value="<?php echo $end ?>">
    <p style="color: red;"><?php echo @$this->error["notime"]["max"] ?></p>
    </div>
    <div>
      <p> So luong toi thieu yeu cau</p>
    <input type="text" name="numberMin" id="numberMin" value="<?php echo @$this->result["min"] ?>">
    <p style="color: red;"><?php echo @$this->error["min"] ?></p>
    </div>
    <div>
      <p>So luong toi da yeu cau</p>
    <input type="text" name="numberMax" id="numberMax" value="<?php echo @$this->result["max"] ?>">
    <p style="color: red;"><?php echo @$this->error["max"] ?></p>
    </div>
    <div>
      <p >Thoi Han DK</p>
    <input type="text" name="time" class="datepicker" value="<?php echo $time ?>">
    <p style="color: red;"><?php echo @$this->error["notime"]["between"] ?></p>
    </div>
    <div style="margin-top: 20px;" >
      <input type="submit" >
    </div>
    <input type="hidden" name = "token"value="<?php echo time(); ?>">


  </form>
</body>
</html>