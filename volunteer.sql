/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `HOATDONG` (
  `MaHD` int(11) NOT NULL AUTO_INCREMENT,
  `TenHD` varchar(255) DEFAULT NULL,
  `MoTaHD` mediumtext DEFAULT NULL,
  `NgayGioBD` date DEFAULT NULL,
  `NgayGioKT` date DEFAULT NULL,
  `SLToiThieuYC` int(11) DEFAULT NULL,
  `SLToiDaYC` int(11) DEFAULT NULL,
  `ThoiHanDK` date DEFAULT NULL,
  `TrangThai` tinyint(4) DEFAULT NULL,
  `MaTV` int(11) DEFAULT NULL,
  `LyDoHuyHD` text DEFAULT NULL,
  PRIMARY KEY (`MaHD`),
  KEY `MaTV` (`MaTV`),
  CONSTRAINT `HOATDONG_ibfk_1` FOREIGN KEY (`MaTV`) REFERENCES `THANHVIEN` (`MaTV`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `THAMGIA` (
  `MaTV` int(11) NOT NULL,
  `MaHD` int(11) NOT NULL,
  `NgayGioDangKy` datetime DEFAULT NULL,
  `DiemTruongDoan` int(11) DEFAULT NULL,
  `DiemTieuChi1` int(11) DEFAULT NULL,
  `DiemTieuChi2` int(11) DEFAULT NULL,
  `DiemTieuChi3` int(11) DEFAULT NULL,
  `NhanXetKhac` text DEFAULT NULL,
  PRIMARY KEY (`MaTV`,`MaHD`),
  KEY `MaHD` (`MaHD`),
  CONSTRAINT `THAMGIA_ibfk_1` FOREIGN KEY (`MaTV`) REFERENCES `THANHVIEN` (`MaTV`) ON DELETE CASCADE,
  CONSTRAINT `THAMGIA_ibfk_2` FOREIGN KEY (`MaHD`) REFERENCES `HOATDONG` (`MaHD`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `THANHVIEN` (
  `MaTV` int(11) NOT NULL AUTO_INCREMENT,
  `HoTen` varchar(255) DEFAULT NULL,
  `GioiTinh` tinyint(4) DEFAULT NULL,
  `NgaySinh` date DEFAULT NULL,
  `DiaChiEmail` varchar(255) DEFAULT NULL,
  `SoDIenThoai` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MaTV`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4;

INSERT INTO `HOATDONG` (`MaHD`, `TenHD`, `MoTaHD`, `NgayGioBD`, `NgayGioKT`, `SLToiThieuYC`, `SLToiDaYC`, `ThoiHanDK`, `TrangThai`, `MaTV`, `LyDoHuyHD`) VALUES
(1, 'Hien mau nhan dao', 'Ngay hoi hien mau toan quoc lan thu 10', '2021-08-01', '2021-08-27', 34, 42, '2021-02-01', 1, 1, NULL);
INSERT INTO `HOATDONG` (`MaHD`, `TenHD`, `MoTaHD`, `NgayGioBD`, `NgayGioKT`, `SLToiThieuYC`, `SLToiDaYC`, `ThoiHanDK`, `TrangThai`, `MaTV`, `LyDoHuyHD`) VALUES
(2, 'Xuan yeu thuong', 'Giup do tre em ngheo o vung nui', '2022-04-01', '2023-08-02', 5, 23, '2022-12-31', 1, 52, NULL);
INSERT INTO `HOATDONG` (`MaHD`, `TenHD`, `MoTaHD`, `NgayGioBD`, `NgayGioKT`, `SLToiThieuYC`, `SLToiDaYC`, `ThoiHanDK`, `TrangThai`, `MaTV`, `LyDoHuyHD`) VALUES
(3, 'Tet am no', 'Giup do nguoi vo gia cu, nguoi ngheo kho co mot cai tet am no', '1970-01-01', '1970-01-01', 5, 13, '1970-01-01', 1, 39, NULL);
INSERT INTO `HOATDONG` (`MaHD`, `TenHD`, `MoTaHD`, `NgayGioBD`, `NgayGioKT`, `SLToiThieuYC`, `SLToiDaYC`, `ThoiHanDK`, `TrangThai`, `MaTV`, `LyDoHuyHD`) VALUES
(4, 'Nhat rac bai bien', 'wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww', '1970-01-01', '1970-01-01', 6, 42, '1970-01-01', 1, 47, NULL),
(5, 'Phat chao o benh vien', 'Phat chao cho benh nhan co hoan canh kho khan', '2022-12-08', '2022-12-16', 5, 42, '2022-12-02', 1, 42, NULL),
(8, 'Hien mau nhan dao lan 2', 'Hien mau nhan dao lan thu 2', '2022-12-29', '2022-12-31', 5, 23, '2022-12-29', 1, 2, NULL);

INSERT INTO `THAMGIA` (`MaTV`, `MaHD`, `NgayGioDangKy`, `DiemTruongDoan`, `DiemTieuChi1`, `DiemTieuChi2`, `DiemTieuChi3`, `NhanXetKhac`) VALUES
(1, 1, '2022-12-27 00:00:00', 5, 5, 5, 3, NULL);
INSERT INTO `THAMGIA` (`MaTV`, `MaHD`, `NgayGioDangKy`, `DiemTruongDoan`, `DiemTieuChi1`, `DiemTieuChi2`, `DiemTieuChi3`, `NhanXetKhac`) VALUES
(1, 2, '2022-12-27 09:59:06', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `THAMGIA` (`MaTV`, `MaHD`, `NgayGioDangKy`, `DiemTruongDoan`, `DiemTieuChi1`, `DiemTieuChi2`, `DiemTieuChi3`, `NhanXetKhac`) VALUES
(1, 3, '2022-12-27 00:00:00', 2, 5, 5, 2, NULL);
INSERT INTO `THAMGIA` (`MaTV`, `MaHD`, `NgayGioDangKy`, `DiemTruongDoan`, `DiemTieuChi1`, `DiemTieuChi2`, `DiemTieuChi3`, `NhanXetKhac`) VALUES
(1, 4, '2022-12-27 00:00:00', 1, 2, 1, 5, NULL),
(1, 8, '2022-12-27 04:47:15', 3, 5, 4, 4, NULL),
(2, 1, '2022-12-27 00:00:00', 3, 5, 2, 5, NULL),
(2, 2, '2022-12-27 00:00:00', 1, 1, 3, 2, NULL),
(2, 3, '2022-12-27 00:00:00', 4, 1, 1, 5, NULL),
(2, 5, '2022-12-27 00:00:00', 4, 3, 2, 1, NULL),
(40, 4, '2022-12-27 00:00:00', 1, 5, 5, 2, NULL),
(43, 2, '2022-12-27 00:00:00', 1, 2, 3, 2, NULL),
(44, 8, '2022-12-27 00:00:00', 4, 4, 2, 4, NULL),
(45, 8, '2022-12-27 00:00:00', 3, 4, 4, 3, NULL),
(47, 3, '2022-12-27 00:00:00', 1, 1, 3, 4, NULL),
(47, 4, '2022-12-27 00:00:00', 3, 2, 4, 1, NULL),
(48, 1, '2022-12-27 00:00:00', 4, 2, 5, 5, NULL),
(49, 3, '2022-12-27 00:00:00', 2, 4, 5, 3, NULL),
(50, 5, '2022-12-27 00:00:00', 3, 5, 4, 2, NULL),
(51, 3, '2022-12-27 00:00:00', 4, 5, 5, 4, NULL),
(51, 4, '2022-12-27 00:00:00', 5, 1, 4, 1, NULL),
(52, 1, '2022-12-27 00:00:00', 5, 4, 2, 1, NULL),
(52, 2, '2022-12-27 00:00:00', 3, 4, 3, 4, NULL),
(52, 3, '2022-12-27 00:00:00', 3, 2, 4, 5, NULL),
(52, 8, '2022-12-27 00:00:00', 1, 1, 1, 3, NULL),
(53, 4, '2022-12-27 00:00:00', 4, 4, 5, 5, NULL),
(57, 1, '2022-12-27 00:00:00', 5, 4, 5, 5, NULL),
(58, 2, '2022-12-27 00:00:00', 3, 3, 5, 2, NULL),
(59, 3, '2022-12-27 00:00:00', 5, 2, 5, 4, NULL),
(60, 2, '2022-12-27 00:00:00', 2, 1, 5, 1, NULL),
(60, 5, '2022-12-27 00:00:00', 5, 5, 2, 3, NULL),
(61, 5, '2022-12-27 00:00:00', 4, 2, 1, 3, NULL),
(62, 1, '2022-12-27 00:00:00', 3, 1, 4, 3, NULL),
(62, 5, '2022-12-27 00:00:00', 2, 5, 1, 5, NULL),
(62, 8, '2022-12-27 00:00:00', 3, 2, 5, 3, NULL),
(65, 4, '2022-12-27 00:00:00', 3, 1, 2, 3, NULL),
(67, 1, '2022-12-27 00:00:00', 2, 2, 4, 3, NULL),
(67, 8, '2022-12-27 00:00:00', 3, 5, 5, 2, NULL);

INSERT INTO `THANHVIEN` (`MaTV`, `HoTen`, `GioiTinh`, `NgaySinh`, `DiaChiEmail`, `SoDIenThoai`) VALUES
(1, 'AAAAAAAAAA', 1, '1997-03-04', 'tuanngoc78919@gmail.com', '982766632793');
INSERT INTO `THANHVIEN` (`MaTV`, `HoTen`, `GioiTinh`, `NgaySinh`, `DiaChiEmail`, `SoDIenThoai`) VALUES
(2, 'sssssssss', 2, '2022-12-02', 'admin@gmail.com', '123456');
INSERT INTO `THANHVIEN` (`MaTV`, `HoTen`, `GioiTinh`, `NgaySinh`, `DiaChiEmail`, `SoDIenThoai`) VALUES
(39, 'cHNV2Cu9', 2, '2022-09-06', 'DrP49XJC@gmail.com', '51198668');
INSERT INTO `THANHVIEN` (`MaTV`, `HoTen`, `GioiTinh`, `NgaySinh`, `DiaChiEmail`, `SoDIenThoai`) VALUES
(40, 'p5gdbF3th', 2, '2022-11-01', 'KbknqCjH@gmail.com', '2848240'),
(41, 'gYTCbJLM5o', 1, '2022-12-11', 'uprSxMXa@gmail.com', '98339615'),
(42, 'ZXmfMDqtQ', 2, '2022-11-06', 'B1nfSuZg@gmail.com', '34147329'),
(43, 'cXQOog', 1, '2022-11-26', 'QfdNFqYr@gmail.com', '54154095'),
(44, '80w5kJB', 1, '2022-11-26', '2lOjIeJr@gmail.com', '30855007'),
(45, '1EWIvb', 1, '2022-12-17', 'MChE8gAD@gmail.com', '66148029'),
(46, '7iAk9 bo', 2, '2022-09-29', 'j7vh4kPl@gmail.com', '97837745'),
(47, 'AHUdQmLcS', 2, '2022-10-29', 'Yf5hbzdi@gmail.com', '14808898'),
(48, 'A fz2gedcT', 1, '2022-10-16', 'Vh6geSJs@gmail.com', '81171784'),
(49, 'rDLj9', 2, '2022-12-04', 'XurC4VnE@gmail.com', '68916113'),
(50, 'uVLGWHQ', 1, '2022-10-20', 'rmynp6lH@gmail.com', '85830942'),
(51, 'M5xJWfO', 1, '2022-12-13', '5arn4w8o@gmail.com', '52279105'),
(52, '8kYF', 2, '2022-11-02', 'YLWhuJbq@gmail.com', '95154237'),
(53, 'jtqf9cm3', 1, '2022-10-21', 'MX6YUWIo@gmail.com', '44966883'),
(54, 'cWbz', 1, '2022-09-26', 'tbJDfRvS@gmail.com', '13937498'),
(55, '05JDLFzR', 1, '2022-11-26', 'cSg8ydM9@gmail.com', '1781157'),
(56, 'JwGvW2VlcZ', 2, '2022-09-26', 'ZEq8zFNx@gmail.com', '63291931'),
(57, 'ha38qFHPNn', 2, '2022-10-01', 'wWC3i6jL@gmail.com', '35777305'),
(58, 'xJMj', 2, '2022-12-18', 'FrmewAI0@gmail.com', '45691603'),
(59, '5Eth', 1, '2022-09-04', 'lFRWD6uL@gmail.com', '33611764'),
(60, 'Sj5IzV0', 1, '2022-11-22', 'n1K3VGhN@gmail.com', '78527962'),
(61, 'RxEN1U', 2, '2022-11-02', 'iVEHnuNd@gmail.com', '31104877'),
(62, 'sxL6IV9Wji', 2, '2022-12-15', 'a4jPdtZ2@gmail.com', '37907115'),
(63, 'jG24F', 2, '2022-12-25', 'wAGXhysf@gmail.com', '2035949'),
(64, 'R8LA5', 1, '2022-11-21', 'iW2Dajpx@gmail.com', '55061127'),
(65, 'keNyK', 2, '2022-09-24', 'F9gHxbuf@gmail.com', '51761231'),
(66, '5aL4BJKUd8', 1, '2022-09-14', 'oY9Br3wZ@gmail.com', '58202524'),
(67, 'jGeD ', 1, '2022-11-14', 'kWv wCud@gmail.com', '60643017'),
(68, 'VL8mQy', 2, '2022-09-29', '8nVh5IiN@gmail.com', '48120072');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;